use std::env;
mod libs;

fn main() {
    let args: Vec<String> = env::args().collect();
    let input_file = &args[1];
    let day: &str = &args[2];

    match day {
        "day1" => libs::day_1::run(input_file),
        _ => day_err(),
    };
    println!("Done")
}

fn day_err() -> u32 {
    println!("Day not available");
    0
}
