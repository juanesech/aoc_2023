extern crate regex;
use super::file_handler;
use regex::Regex;

pub fn run(input_file: &str) -> u32 {
    let mut numbers: Vec<u32> = Vec::new();
    if let Ok(lines) = file_handler::read_lines(input_file) {
        for line in lines {
            if let Ok(text) = line {
                numbers.push(get_numbers(text));
            }
        }
    }
    let sum: u32 = numbers.iter().sum();
    println!("Input length: {:?}", numbers.len());
    println!("Calibration value sum: {:?}", sum);
    sum
}

fn get_numbers(text: String) -> u32 {
    let mut nums: Vec<u32> = Vec::new();
    println!("INITIAL TEXT -> {text}");
    let mut num_tuples = find_written_number(&text);
    num_tuples.sort_by(|a, b| a.1.cmp(&b.1));
    for n in num_tuples {
        println!("NUM: {:?}, POSIN: {:?}, POSFIN: {:?}", n.0, n.1, n.2);
        nums.push(n.0);
    }
    if nums.is_empty() {
        0
    } else {
        let joined_number: String =
            nums.first().unwrap().to_string() + &nums.last().unwrap().to_string();
        let result: u32 = joined_number.parse().unwrap();
        println!("RESULT -> {:?}", result);
        println!("-----------------------------------");
        result
    }
}

fn number_text_translate(text: &str) -> u32 {
    match text {
        "one" => 1,
        "two" => 2,
        "three" => 3,
        "four" => 4,
        "five" => 5,
        "six" => 6,
        "seven" => 7,
        "eight" => 8,
        "nine" => 9,
        _ => 0,
    }
}

fn find_written_number(text: &str) -> Vec<(u32, usize, usize)> {
    let reg = Regex::new(r"one|two|three|four|five|six|seven|eight|nine|\d").unwrap();
    let mut numbers: Vec<(u32, usize, usize)> = Vec::new();

    for mat in reg.find_iter(text) {
        let mstart = mat.start();
        let mend = mat.end();
        let mut number = number_text_translate(&text[mstart..mend]);
        if let Some(num) = text.chars().nth(mstart) {
            if num.is_ascii_hexdigit() {
                if let Some(n) = num.to_digit(10) {
                    number = n;
                }
            }
        }
        numbers.push((number, mstart, mend));
    }
    numbers
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_get_numbers() {
        let input_text = String::from("eighttesting245");
        let number = get_numbers(input_text);
        let expected_number = 85;
        assert_eq!(expected_number, number);
    }

    #[test]
    fn test_number_text_translate() {
        let text_number = "one";
        let number: u32 = number_text_translate(text_number);
        assert_eq!(number, 1);
    }

    #[test]
    fn test_day1_run() {
        let input_file = "input.test";
        let sum = run(input_file);
        let expected_value = 513;
        assert_eq!(sum, expected_value);
    }
}
